CC = cl
RM = del
TARGETS = defs app utils input draw text audio window game
OBJECTS = build\objs\*.obj
INCLUDES = include
COMPILER_FLAGS_D = /D_DEBUG /D_CRT_SECURE_NO_WARNINGS /wd4142 /I $(INCLUDES) /W3 /EHsc /c /Zi /Od /Tp
COMPILER_FLAGS = /D_CRT_SECURE_NO_WARNINGS /wd4142 /I $(INCLUDES) /W3 /EHsc /c /Zi /Ox /Tp 
LINKER_FLAGS_D = /SUBSYSTEM:CONSOLE /LIBPATH:lib
LINKER_FLAGS = /SUBSYSTEM:WINDOWS /LIBPATH:lib
LIBS = SDL2.lib SDL2main.lib SDL2_image.lib SDL2_ttf.lib SDL2_mixer.lib opengl32.lib glew32.lib glew32s.lib freetype.lib

EXE_NAME = Cephalopod.exe

.default: all

.PHONY: run

all: $(TARGETS) link

link:
	LINK $(LINKER_FLAGS_D) $(OBJECTS) $(LIBS) /OUT:build\$(EXE_NAME)

defs:
	$(CC) $(COMPILER_FLAGS) src\LMGF\defs.cpp /Fobuild\objs\defs.obj 

app:
	$(CC) $(COMPILER_FLAGS) src\LMGF\app.cpp /Fobuild\objs\app.obj 

utils:
	$(CC) $(COMPILER_FLAGS) src\LMGF\utils.cpp /Fobuild\objs\utils.obj 

input:
	$(CC) $(COMPILER_FLAGS) src\LMGF\input.cpp /Fobuild\objs\input.obj 

draw:
	$(CC) $(COMPILER_FLAGS) src\LMGF\draw.cpp /Fobuild\objs\draw.obj 

text:
	$(CC) $(COMPILER_FLAGS) src\LMGF\text.cpp /Fobuild\objs\text.obj 

audio:
	$(CC) $(COMPILER_FLAGS) src\LMGF\audio.cpp /Fobuild\objs\audio.obj 

window:
	$(CC) $(COMPILER_FLAGS) src\LMGF\window.cpp /Fobuild\objs\window.obj 

game:
	$(CC) $(COMPILER_FLAGS) src\game.cpp /Fobuild\objs\game.obj 

run:
	cd build && $(EXE_NAME)
