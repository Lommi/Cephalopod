#include "../src/LMGF/app.h"

#include <sstream>

#define ENEMY_ALLOCATIONS 30
#define DEF_PLAYER_MAXSPEED 30
#define DEF_PLAYER_ACCELERATION 7.0f
#define DEF_ENEMYSPEED 15
#define MAX_ENEMYSPEED 20
#define DEF_ENEMYSPEED_SCALER 1
#define DEF_SPAWNRATE 8
#define SCORE_TEXT_OFFSET 64
#define SPARKLE_ALLOCATIONS 6
#define SCORE_EFFECT_TIMER_CAP 3.0f
#define SAVEDATA_LEN 2

void Init();
void Reset();
void ResetBall();
void CheckScore();
void Update();
void PreRender();
void Render();
void KeyDown();
void KeyUp();
void FingerDown();
void FingerUp();
void FingerMotion();

void GameManagerUpdate();
void PlayerUpdate();
void EnemiesUpdate();
void ScoreObjUpdate();
void SparklesUpdate();
void ScoreEffectUpdate();

void Reset();
void TurnLeft();
void TurnRight();
void GameOver();
void DrawGame();
void DrawMenuText();
void SaveHighscore();
void UpdateRecords();

struct SaveData;
struct Timer;
class GameManager;
class UIRect;
class baseObj;
class Player;
class GameObj;
class Projectile;
class GameManager;

enum GameState
{
    GS_MENU = 0,
    GS_GAME,
    GS_GAMEOVER
};

struct SaveData
{
    uint highScore;
    uint highCombo;
};

struct Timer
{
    int current;
    int cap;
};

class GameManager
{
public:
    GameManager(){(srand((unsigned int)time(0)));};
    uint score;
    uint scoreMultiplier;
    uint hiscore;
    uint prevHiscore;
    uint comboRecord;
    SaveData savedata;
    bool shouldSaveHighscore = false;
    int spawnIndex;
    Timer scoreSoundTracker;
    Timer spawnTimer;
    Timer spawnTrigger;
};

class baseObj
{
public:
    bool alive;
    uint8_t animFrame;          //Current animation frame number
    uint8_t animFrameCounter;   //Counts how long the frame lasts
    fvec2 pos;
    frect collider;
    SDL_Color *color;
    Texture *tex;
    Animation *anim;

    void init(float posx, float posy, float width, float height, SDL_Color *col);
    void setPos(float x, float y);
};

class Player : public baseObj
{
public:
    bool leftPressed;
    bool rightPressed;
    fvec2 dir;
    float speedMultiplier;
    float acceleration;
    float maxSpeed;
    float angle;
};

class GameObj : public baseObj
{
public:
    GameObj();
    short type;
    float speed;
    float dir;
};

App app;
Input input;
Renderer renderer;

int gamestate = GS_MENU;

bool scoreEffectActive = false;
fvec2 scoreEffectPos;
float scoreEffectTimer;

float enemySpeed = DEF_ENEMYSPEED;
float sparkleGravity = 0.0f;
int newComboScore = 0;

Font *fontConsola;
Font *fontScore;

SoundClip *scoreSound;
SoundClip *deadSound;
SoundClip *selectSound;
SoundClip *deleteSound;
SoundClip *destroySound;

SpriteFlip playerFlip;

Texture menuTapToStartText;
Texture menuHighScoreText;
Texture menuHighScoreNumberText;
Texture menuComboRecordText;
Texture menuComboRecordNumberText;

Texture comboText;
Texture scoreText;
Texture scoreEffectText;
Texture gameOverText;

Texture texPlayer;
Texture texPlayerDead;
Texture texCoin;
Texture texCoinEffect;
Texture texFireball;
Texture texSparkle;

GameManager gameManager;
Player player;
GameObj scoreObj;
GameObj enemies[ENEMY_ALLOCATIONS];
GameObj sparkles[SPARKLE_ALLOCATIONS];

Animation anim;
Frame frames[3];
Frame frameSparkle;
Frame frameBackground;

char *bufScoreText;
char *bufHiscoreText;
char *bufComboText;
char *bufComboRecordText;
char *bufScoreEffectText;

SDL_Color colorDarkBlue = { 10 , 25, 50, 255 };

int main(int argc, char **argv)
{
    App_init(&app, "Cephalopod", "../src/LMGF", "../assets/");
    Init();
    App_mainloop(&app, &input, &renderer);

    return 0;
}

void
Init()
{
    app.Update         = Update;
    renderer.PreRender = PreRender;
    renderer.Render    = Render;
    input.FingerDown   = FingerDown;
    input.KeyDown      = KeyDown;

    Window_setColor(&app.window, 0, 0, 0, 1);

    Audio_setAllVolume(15);

    fontConsola = Text_loadFont("GermaniaOne-Regular.TTF", 72);
    fontScore   = Text_loadFont("GermaniaOne-Regular.TTF", 48);

    scoreSound   = Audio_loadSound("score.wav");
    deadSound    = Audio_loadSound("dead.wav");
    selectSound  = Audio_loadSound("select.wav");
    deleteSound  = Audio_loadSound("delete.wav");
    destroySound = Audio_loadSound("destroy.wav");

    gameManager.spawnTimer.cap = DEF_SPAWNRATE;
    gameManager.score = 0;
    gameManager.hiscore = 0;
    gameManager.comboRecord = 0;
    gameManager.scoreMultiplier = 0;
    gameManager.scoreSoundTracker.cap = 0;

    int savedata[SAVEDATA_LEN];
    if (!Utils_readFile("Lommi", "Cephalopod", "savedata.bin", savedata, sizeof(int) * SAVEDATA_LEN))
    {
        SaveHighscore();
    }
    else
    {
        gameManager.hiscore = savedata[0];
        gameManager.comboRecord = savedata[1];
    }

    Draw_loadTexture(&texPlayer,     "player.png");
    Draw_loadTexture(&texPlayerDead, "playerDead.png");
    Draw_loadTexture(&texFireball,   "enemy.png");
    Draw_loadTexture(&texSparkle,    "sparkle.png");
    Draw_loadTexture(&texCoin,       "star.png");

    frameBackground.rect[0] = 0;
    frameBackground.rect[1] = 0;
    frameBackground.rect[2] = (uint)app.window.viewport[2];
    frameBackground.rect[3] = (uint)app.window.viewport[3];

    bufScoreText        = (char*)malloc(sizeof(char));
    bufHiscoreText      = (char*)malloc(sizeof(char));
    bufScoreEffectText  = (char*)malloc(sizeof(char));
    bufComboText        = (char*)malloc(sizeof(char));

    char *tempHighscoreText = IntToString(bufHiscoreText, gameManager.hiscore);
    char *tempComboRecordText = IntToString(bufComboText, gameManager.comboRecord);

    Text_create(&menuTapToStartText, "TAP TO START", fontConsola, colorWhite, 0);
    Text_create(&menuHighScoreText, "HIGHSCORE: ", fontConsola, colorWhite, 0);
    Text_create(&menuComboRecordText, "COMBO RECORD: ", fontConsola, colorWhite, 0);
    Text_create(&gameOverText, "GAME OVER", fontConsola, colorWhite, 0);
    Text_create(&menuHighScoreNumberText, tempHighscoreText, fontConsola, colorYellow, 0);
    Text_create(&menuComboRecordNumberText, tempComboRecordText, fontConsola, colorAqua, 0);
    Text_create(&scoreText, IntToString(bufScoreText, 0), fontScore, colorYellow, 1);
    Text_create(&comboText, IntToString(bufComboText, 0), fontScore, colorAqua, 1);

    playerFlip = SPRITE_FLIP_NONE;

    anim.frameCount = 3;
    for (uint i = 0; i < anim.frameCount; ++i)
    {
        frames[i].rect[0] = i * 64;
        frames[i].rect[1] = 0;
        frames[i].rect[2] = 64;
        frames[i].rect[3] = 64;
        frames[i].length  = 8;
    }

    anim.frames = frames;
    player.tex = &texPlayer;
    player.init(app.window.viewport[2] / 2 - (player.tex->width / 2), app.window.viewport[3] - 80, 64, 64, &colorGreen);
    player.acceleration = DEF_PLAYER_ACCELERATION;
    player.maxSpeed = DEF_PLAYER_MAXSPEED;
    player.anim = &anim;

    for (int i = 0; i < ENEMY_ALLOCATIONS; ++i)
    {
        enemies[i].setPos((float)(rand() % (int)(app.window.viewport[2] - texFireball.width)), -64);
        enemies[i].tex = &texFireball;
        enemies[i].anim = &anim;
        enemies[i].collider.w = 64;
        enemies[i].collider.h = 64;
    }

    scoreObj.init(((app.window.viewport[2] / 2) - app.window.viewport[2] / 3 + rand() % (int)app.window.viewport[2] / 2), -64, 64, 64, &colorBlue);
    scoreObj.alive = 1;
    scoreObj.tex = &texCoin;
    scoreObj.anim = &anim;

    frameSparkle.rect[0] = 0;
    frameSparkle.rect[1] = 0;
    frameSparkle.rect[2] = texSparkle.width;
    frameSparkle.rect[3] = texSparkle.height;

    for (int i = 0; i < SPARKLE_ALLOCATIONS; ++i)
    {
        sparkles[i].tex = &texSparkle;
        sparkles[i].collider.w = 14;
        sparkles[i].collider.h = 14;
    }
}

void
PreRender()
{
    Draw_rectangleGradient(0, 0, app.window.viewport[2], app.window.viewport[3],
        colorDarkBlue, colorDarkBlue, colorBlack, colorBlack, colorBlack);
}

void
Render()
{
    switch (gamestate)
    {
        case GS_MENU:
        {
            Draw_sprite(&menuTapToStartText, app.window.viewport[2] / 2 - menuTapToStartText.width / 2, app.window.viewport[3] / 2 - 200, 0);
            DrawMenuText();
            Draw_sprite(player.tex, player.pos.x, player.pos.y, player.anim->frames[player.animFrame].rect, playerFlip);
        }break;
        case GS_GAME:
        {
            DrawGame();
        }break;
        case GS_GAMEOVER:
        {
            DrawGame();
            DrawMenuText();
            Draw_sprite(&gameOverText, app.window.viewport[2] / 2 - gameOverText.width / 2, app.window.viewport[3] / 2 - 200, 0);
        }break;
    }
}

void
Update()
{
    player.animFrameCounter += 1;
    if (player.animFrameCounter >= player.anim->frames[player.animFrame].length)
    {
        ++player.animFrame;
        if (player.animFrame >= 2)
        {
            player.animFrame = 0;
        }
        player.animFrameCounter = 0;
    }
    switch (gamestate)
    {
        case GS_GAME:
        {
            GameManagerUpdate();
            PlayerUpdate();
            ScoreObjUpdate();
            EnemiesUpdate();
            SparklesUpdate();
            ScoreEffectUpdate();
        }break;
    }
}

void GameManagerUpdate()
{
    ++gameManager.spawnTimer.current;
    if (gameManager.spawnTimer.current >= gameManager.spawnTimer.cap)
    {
        if (!scoreObj.alive)
        {
            scoreObj.alive = 1;
        }
        else
        {
            enemies[gameManager.spawnIndex].alive = 1;
            ++gameManager.spawnIndex;
            if (gameManager.spawnIndex > ENEMY_ALLOCATIONS)
                gameManager.spawnIndex = 0;
        }
        gameManager.spawnTimer.current = 0;
    }
}

void PlayerUpdate()
{
    player.dir.x = Approach(player.speedMultiplier, player.dir.x, player.acceleration);
    VecAdd(&player.pos, &player.dir);

    if (player.pos.x < 0)
    {
        player.pos.x = 0;
    }
    if (player.pos.x > app.window.viewport[2] - player.collider.w)
    {
        player.pos.x = app.window.viewport[2] - player.collider.w;
    }

    player.collider.x = player.pos.x;
    player.collider.y = player.pos.y;

}

void EnemiesUpdate()
{
    for (int i = 0; i < ENEMY_ALLOCATIONS; ++i)
    {
        if (!enemies[i].alive)
        {
            continue;
        }

        enemies[i].setPos(enemies[i].pos.x, enemies[i].pos.y + enemySpeed);
        enemies[i].collider.x = enemies[i].pos.x;
        enemies[i].collider.y = enemies[i].pos.y;

        ++enemies[i].animFrameCounter;
        if (enemies[i].animFrameCounter > enemies[i].anim->frames[enemies[i].animFrame].length)
        {
            ++enemies[i].animFrame;
            if (enemies[i].animFrame >= 3)
            {
                enemies[i].animFrame = 0;
            }
            enemies[i].animFrameCounter = 0;
        }

        if (enemies[i].pos.y < app.window.viewport[3] - enemies[i].collider.h * 2)
        {
            continue;
        }

        if (enemies[i].pos.y + enemies[i].collider.h > app.window.viewport[3])
        {
            enemies[i].pos.y = -enemies[i].collider.h;
            enemies[i].pos.x = (float)(rand() % (int)(app.window.viewport[2] - enemies[i].collider.w));
            enemies[i].alive = 0;
            if (gamestate == GS_GAME)
            {
                gameManager.score += 10;
                Text_create(&scoreText, IntToString(bufScoreText, gameManager.score), fontScore, colorYellow, 0);
                UpdateRecords();
            }
        }

        if (AABB(&player.collider, &enemies[i].collider) && player.alive)
        {
            GameOver();
        }
    }
}

void ScoreObjUpdate()
{
    if (!scoreObj.alive)
    {
        return;
    }

    scoreObj.setPos(scoreObj.pos.x, scoreObj.pos.y + enemySpeed);

    if (scoreObj.pos.y < app.window.viewport[3] - scoreObj.collider.h * 2)
    {
        return;
    }

    if (AABB(&player.collider, &scoreObj.collider))
    {
        scoreObj.alive = 0;

        if (gamestate == GS_GAME)
        {
            ++gameManager.scoreMultiplier;
            gameManager.score += 100 * (gameManager.scoreMultiplier);

            if (enemySpeed < MAX_ENEMYSPEED)
            {
                enemySpeed += DEF_ENEMYSPEED_SCALER;
            }

            Audio_playSound(scoreSound);

            sparkleGravity = 0.0f;

            Text_create(&scoreEffectText, IntToString(bufScoreEffectText, (100 * (gameManager.scoreMultiplier))), fontScore, colorAqua, 0);

            Text_create(&scoreText, IntToString(bufScoreText, gameManager.score), fontScore, colorYellow, 0);

            UpdateRecords();

            Text_create(&comboText, IntToString(bufHiscoreText, gameManager.scoreMultiplier), fontScore, colorAqua, 0);

            for (int i = 0; i < SPARKLE_ALLOCATIONS; ++i)
            {
                sparkles[i].alive = 1;
                sparkles[i].dir = (float)(-45 - rand() % 90);
                sparkles[i].speed = (float)(10 + rand() % 5);
                sparkles[i].pos.x = scoreObj.pos.x;
                sparkles[i].pos.y = scoreObj.pos.y - 16;
            }
        }

        scoreEffectActive = true;
        scoreEffectTimer = 0.0f;
        scoreEffectPos.x = player.pos.x;
        scoreEffectPos.y = player.pos.y - 96;

        scoreObj.setPos((scoreObj.pos.x - 600 + rand() % 1200), -scoreObj.collider.h);
        if (scoreObj.pos.x + scoreObj.collider.w > app.window.viewport[2])
        {
            scoreObj.setPos((scoreObj.pos.x - 600), scoreObj.pos.y);
        }
        if (scoreObj.pos.x < 0)
        {
            scoreObj.setPos((scoreObj.pos.x + 600), scoreObj.pos.y);
        }
    }

    if (scoreObj.pos.y + scoreObj.collider.h > app.window.viewport[3] + scoreObj.collider.h)
    {
        scoreObj.alive = 0;
        scoreObj.setPos((scoreObj.pos.x - 600 + rand() % 1200), -scoreObj.collider.h);
        if (scoreObj.pos.x + scoreObj.collider.w > app.window.viewport[3])
        {
            scoreObj.setPos((scoreObj.pos.x - 600), scoreObj.pos.y);
        }
        if (scoreObj.pos.x < 0)
        {
            scoreObj.setPos((scoreObj.pos.x + 600), scoreObj.pos.y);
        }

        Audio_playSound(destroySound);

        gameManager.scoreMultiplier = 0;

        enemySpeed = DEF_ENEMYSPEED;

        Text_create(&comboText, IntToString(bufHiscoreText, gameManager.scoreMultiplier), fontScore, colorAqua, 0);
    }
}

void SparklesUpdate()
{
    if (sparkleGravity < 100.0f)
    {
        sparkleGravity += 1.0f;
    }

    for (int i = 0; i < SPARKLE_ALLOCATIONS; ++i)
    {
        if (!sparkles[i].alive)
        {
            continue;
        }

        sparkles[i].pos.x += (float)(cos(sparkles[i].dir * RADIAN) * sparkles[i].speed);
        sparkles[i].pos.y += (float)(sin(sparkles[i].dir * RADIAN) * sparkles[i].speed + sparkleGravity);

        sparkles[i].collider.x = sparkles[i].pos.x;
        sparkles[i].collider.y = sparkles[i].pos.y;

        if (sparkles[i].pos.x < 0 || sparkles[i].pos.x >
            app.window.viewport[2] - sparkles[i].tex->width)
        {
            sparkles[i].alive = 0;
        }

        if (sparkles[i].pos.y + sparkles[i].collider.h > app.window.viewport[3])
        {
            sparkles[i].alive = 0;
        }
    }
}

void ScoreEffectUpdate()
{
    if (!scoreEffectActive)
    {
        return;
    }

    scoreEffectTimer += 0.1f;
    if (scoreEffectTimer > SCORE_EFFECT_TIMER_CAP)
    {
        scoreEffectActive = false;
    }
}

void Reset()
{
    auto screenW = app.window.viewport[2];
    gameManager.prevHiscore = gameManager.hiscore;
    gameManager.score = 0;
    gameManager.spawnIndex = 0;
    gameManager.spawnTimer.current = 0;
    gameManager.scoreMultiplier = 0;

    player.alive = 1;
    player.pos.x = screenW / 2 - (player.tex->width / 2);

    player.leftPressed = 0;
    player.rightPressed = 0;
    player.speedMultiplier = 0;
    player.angle = 0;
    player.tex = &texPlayer;

    newComboScore = 0;

    for (int i = 0; i < SPARKLE_ALLOCATIONS; ++i)
    {
        sparkles[i].alive = 0;
    }

    scoreObj.setPos(((screenW / 2) - screenW / 4 + rand() % (int)screenW / 3),
        -scoreObj.collider.h);
    scoreObj.animFrame = 0;
    scoreObj.alive = 1;

    sparkleGravity = 0.0f;

    for (int i = 0; i < ENEMY_ALLOCATIONS; ++i)
    {
        enemies[i].alive = 0;
        enemies[i].pos.x = (float)(rand() % (int)(screenW - enemies[i].collider.w));
        enemies[i].pos.y = -enemies[i].collider.h;
    }

    enemySpeed = DEF_ENEMYSPEED;

    Text_create(&scoreText, IntToString(bufScoreText, gameManager.score), fontScore, colorYellow, 0);
    Text_create(&comboText, IntToString(bufComboText, gameManager.scoreMultiplier), fontScore, colorAqua, 0);

    gamestate = GS_GAME;
}

void TurnLeft()
{
    player.speedMultiplier = -player.maxSpeed;
    player.leftPressed = 1;
    playerFlip = SPRITE_FLIP_HORIZONTAL;
}

void TurnRight()
{
    player.speedMultiplier = player.maxSpeed;
    player.rightPressed = 1;
    playerFlip = SPRITE_FLIP_NONE;
}

void UpdateRecords()
{
    if (gameManager.score > gameManager.hiscore)
    {
        Text_create(&menuHighScoreNumberText, IntToString(bufScoreText, gameManager.score), fontConsola, colorYellow, 0);
        gameManager.hiscore = gameManager.score;
        gameManager.shouldSaveHighscore = true;
    }

    if (gameManager.scoreMultiplier > gameManager.comboRecord)
    {
        Text_create(&menuComboRecordNumberText, IntToString(bufHiscoreText, gameManager.scoreMultiplier), fontConsola, colorAqua, 0);
        gameManager.comboRecord = gameManager.scoreMultiplier;
        gameManager.shouldSaveHighscore = true;
    }
}

void GameOver()
{
    player.tex = &texPlayerDead;
    gamestate = GS_GAMEOVER;
    scoreEffectActive = false;
    Audio_playSound(deadSound);
    if (gameManager.shouldSaveHighscore)
    {
        SaveHighscore();
    }
    gameManager.shouldSaveHighscore = false;
}

void DrawGame()
{
    Draw_sprite(&scoreText, app.window.viewport[2] - SCORE_TEXT_OFFSET - scoreText.width, 64, 0);
    Draw_sprite(&comboText, app.window.viewport[2] - SCORE_TEXT_OFFSET - comboText.width, 128, 0);

    Draw_sprite(player.tex, player.pos.x, player.pos.y, player.anim->frames[player.animFrame].rect, playerFlip);

    if (scoreObj.alive)
    {
        Draw_sprite(scoreObj.tex, scoreObj.pos.x, scoreObj.pos.y, anim.frames[0].rect);
    }

    for (int i = 0; i < ENEMY_ALLOCATIONS; ++i)
    {
        if (!enemies[i].alive)
        {
            continue;
        }
        Draw_sprite(enemies[i].tex, enemies[i].pos.x, enemies[i].pos.y, anim.frames[enemies[i].animFrame].rect);
    }

    if (scoreEffectActive)
    {
        Draw_sprite(&scoreEffectText, scoreEffectPos.x, scoreEffectPos.y, 0);
    }

    for (int i = 0; i < SPARKLE_ALLOCATIONS; ++i)
    {
        if (!sparkles[i].alive)
        {
            continue;
        }
        Draw_sprite(sparkles[i].tex, sparkles[i].pos.x, sparkles[i].pos.y, frameSparkle.rect);
    }
}

void
DrawMenuText()
{
    Draw_sprite(&menuHighScoreText, app.window.viewport[2] / 2 - menuHighScoreText.width / 2 - 124, app.window.viewport[3] / 2, 0);
    Draw_sprite(&menuComboRecordText, app.window.viewport[2] / 2 - menuComboRecordText.width / 2 - 64, app.window.viewport[3] / 2 + 100, 0);
    Draw_sprite(&menuHighScoreNumberText, (app.window.viewport[2] / 2) + 180, app.window.viewport[3] / 2, 0);
    Draw_sprite(&menuComboRecordNumberText, (app.window.viewport[2] / 2) + 180, app.window.viewport[3] / 2 + 100, 0);
}

void SaveHighscore()
{
    int savedata[SAVEDATA_LEN];
    savedata[0] = gameManager.hiscore;
    savedata[1] = gameManager.comboRecord;
    Utils_writeFile("Lommi", "Cephalopod", "savedata.bin", savedata, sizeof(int) * SAVEDATA_LEN);
}

void baseObj::init(float posx, float posy, float width, float height, SDL_Color *col)
{
    pos.x = posx;
    pos.y = posy;

    collider.x = posx;
    collider.y = posy;
    collider.w = width;
    collider.h = height;
    color = col;
    alive = 1;
}

void baseObj::setPos(float x, float y)
{
    pos.x = x;
    pos.y = y;
    collider.x = x;
    collider.y = y;
}

GameObj::GameObj()
{
    init((float)(rand() % (int)(app.window.viewport[3] - 64)), -64,
        64, 64, &colorRed);
    dir = 90;
    speed = enemySpeed;
    alive = 0;
}

void
KeyDown()
{
    switch (input.keyboardInput)
    {
        case SDLK_ESCAPE:
        {
            UpdateRecords();
            App_shutDown(&app);
        }break;
        case SDLK_a:
        case SDLK_LEFT:
        {
            TurnLeft();
            break;
        }
        case SDLK_d:
        case SDLK_RIGHT:
        {
            TurnRight();
        }break;
    }

    if (input.keyboardInput == SDLK_SPACE && (gamestate == GS_MENU || gamestate == GS_GAMEOVER))
    {
        Reset();
    }
}

void
FingerDown()
{
    switch (gamestate)
    {
        case GS_MENU: gamestate = GS_GAME; break;
        case GS_GAMEOVER: Reset(); break;
        case GS_GAME:
        {
            if (input.touchInputs[input.fingerIndex].position.x < app.window.viewport[2] / 2)
            {
                TurnLeft();
            }
            else
            {
                TurnRight();
            }
        }break;
    }
}
